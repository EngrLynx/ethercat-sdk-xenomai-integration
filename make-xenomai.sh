#! /bin/bash

export HOST=arm-linux-gnueabihf
export CWD=`pwd`
export MAKEDIR=/home/dev/${SRC_REL_PATH}/xenomai
export ARCH=arm
export CROSS_COMPILE=${HOST}-
export FSDIR=/home/dev/${SRC_REL_PATH}/rootfs
export DESTDIR=${FSDIR}/output/target

if ! ${CROSS_COMPILE}gcc --version 2>/dev/null; then
    echo "Cross compiler not found. Make sure you are inside the SDK container."
    exit 1
fi

if [ ! -d "${MAKEDIR}" ]; then
    echo "Directory to be built ${MAKEDIR} not found."
    exit 1
fi

if [ ! -d "${FSDIR}" ]; then
    echo "Target filesystem directory ${FSDIR} not found."
    exit 1
fi

if [ ! -d "${DESTDIR}" ]; then
    echo "Destination directory ${DESTDIR} not found. Make sure rootfs is already built."
    exit 1
fi

# Make xenomai
cd ${MAKEDIR}
source /home/dev/${SDK_REL_PATH}/linux-devkit/environment-setup
./configure --build=x86_64 --host=${HOST} --with-core=mercury
make install
rm -rf ${DESTDIR}/usr/xenomai/share/doc
# Make and deploy rootfs where test binaries are placed
cd ${FSDIR}
make
make deploy
cd ${CWD}
echo "Build successful."
