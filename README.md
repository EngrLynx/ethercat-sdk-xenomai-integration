## How To Use:

1. Download this repo by running `git clone git@bitbucket.org/EngrLynx/ethercat-sdk-xenomai-integration.git`.
2. Download [Xenomai](https://xenomai.org/downloads/xenomai/stable/).
3. Extract Xenomai into a folder named `xenomai` inside the `src` folder of the [Ethercat SDK Container](https://bitbucket.org/EngrLynx/ethercat-sdk-container).
4. Move `make-xenomai.sh` to the `src` folder.
